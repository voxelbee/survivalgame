// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Survival_Game_2GameMode.generated.h"

UCLASS(minimalapi)
class ASurvival_Game_2GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASurvival_Game_2GameMode();
};



