// Fill out your copyright notice in the Description page of Project Settings.

#include "ChunkManager.h"
#include "Survival_Game_2.h"
#include "Core/Chunks/ChunkDataContainer.h"
#include "Core/Threading/ThreadTaskGraph.h"
#include "Core/Chunks/Chunk.h"
#include <algorithm>
#include <iostream>

ChunkManager::ChunkManager(ThreadTaskGraph* TASK_GRAPH_IN, UWorld* IN_WORLD, int32 IN_SEED, int32 IN_CHUNK_OCTAVES, int32 IN_CHUNK_RESOLUTION, int32 IN_VIEW_DISTANCE, float IN_ZOOM_LEVEL)
{
	//Set up defaults
	taskManager = TASK_GRAPH_IN;
	world = IN_WORLD;
	seed = IN_SEED;
	chunk_octaves = IN_CHUNK_OCTAVES;
	chunk_resolution = IN_CHUNK_RESOLUTION;
	view_distance = IN_VIEW_DISTANCE;
	zoom_level = IN_ZOOM_LEVEL;

	//Set this to 0 because it needs something to be able to ccompare against at the first tick
	old_player_location = FVector(MAX_FLT, MAX_FLT, MAX_FLT);

	old_player_chunk_location = FVector2D(MAX_FLT, MAX_FLT);
}

ChunkManager::~ChunkManager()
{

}

/*
* An updater called every frame to remove chunks from the world
* @param In_DeltaTime - The delta time of the frame
*/
void ChunkManager::Update(float IN_DeltaTime)
{

	//Geting the player character from the world useing the index
	//it is 0 because that is the only player on the game at the moment.
	ACharacter* player = UGameplayStatics::GetPlayerCharacter(world, 0);

	if (player == NULL)
	{
		return;
	}

	//Geting the current player location.
	FVector player_location = player->GetActorLocation();

	if (player_location != old_player_location)
	{
		//Working out the location of the player in chunk loation
		//eg 1600 in world units is 1 in chunk units
		FVector2D player_chunk_location = FVector2D(floor(player_location.X / 3200), floor(player_location.Y / 3200));
		if (player_chunk_location != old_player_chunk_location)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Chunk location change!"));
			SpiralGeneration(player_chunk_location, 20);
			RemoveChunksUpdate(player_chunk_location, 20);
			old_player_chunk_location = player_chunk_location;
			world->ForceGarbageCollection(true);
		}
		old_player_location = player_location;
	}
}

/*
* Spawns a chunk into the world
* @param IN_Location - The location of the chunk in world units
* @return The pointer to the chunk
*/
AChunk* ChunkManager::SpawnChunk(FVector &IN_Location)
{
	//Spawning the actor useing the world variable set in the constructor
	AChunk* chunk = (AChunk*)world->SpawnActor(AChunk::StaticClass(), &IN_Location);

	return chunk;
}

/*
* A simple function to create a chunk and add it to the world
* @param IN_Octaves - the octaves of the height map to be generated
* @param IN_TerrainSize - The amount of vertices the chunk will have allong one side eg. 16 will be 16 x 16
* @param IN_ChunkLocation - The location of the chunk in chunk units
* @param IN_IsNewChunk - True if you whant to spawn a new chunk
*/
void ChunkManager::CreateChunk(int32 IN_Octaves, int32 IN_TerrainSize, FVector2D IN_ChunkLocation, bool IN_IsNewChunk)
{
	if (IN_IsNewChunk)
	{
		//Work out the loaction of the chunk in 3d space
		//eg 1 chunk unit is 1600 world units
		FVector location3d = FVector(IN_ChunkLocation.X * 3200, IN_ChunkLocation.Y * 3200, 1000);
		//Sapwn the chunk
		currentChunk = SpawnChunk(location3d);

		//Add the chunk to the map
		chunkMap_Size_1.Emplace(IN_ChunkLocation, currentChunk);
	}
	else
	{
		currentChunk = chunkMap_Size_1.FindRef(IN_ChunkLocation);
	}
	//We need to call a method outside of this class to create the mesh object beacuse this class is not an actor
	//UProceduralMeshComponent* mesh = NewObject<UProceduralMeshComponent>(currentChunk);

	//Create the chunk mesh and the container that will go to the thread and work out the mesh vertices ect...
	//ChunkDataContainer* chunkContainer = NewObject<ChunkDataContainer>();
	ChunkDataContainer* chunkContainer = new ChunkDataContainer();

	//Zoomlevel is the amount of zoom on the chunk height map
	chunkContainer->zoomLevel = zoom_level;
	//TerrainSize mostly efects the quality of the chunk it is how meany pixels in width/height the height map will be
	//and how many vertices the finshed mesh will have in width/height
	chunkContainer->terrainSizeL = IN_TerrainSize;
	//The location of the chunk in chunk units.
	chunkContainer->chunkLocation = IN_ChunkLocation;
	//TerrainOctaves sets the amount of octaves on the heightmap the lower the number the less quality but faster generation
	chunkContainer->terrainOctaves = IN_Octaves;
	//The referance the the chunk just spawned
	chunkContainer->chunk = currentChunk;
	//The mesh to generate into
	chunkContainer->chunkSeed = seed;
	//Send the task to generate the chunk
	taskManager->AddTask(chunkContainer);
}

/*
* An updater called every frame to remove chunks from the world
*/
void ChunkManager::RemoveChunksUpdate(FVector2D IN_Location, int32 IN_Size)
{
	for (const auto& Entry : chunkMap_Size_1)
	{
		float distance = Entry.Key.Distance(IN_Location, Entry.Key);
		distance = abs(distance);
		if (distance >= IN_Size)
		{
			AddToPendingChunks(Entry.Key);
		}
	}

	if (!pendingChunksToDestroy.IsEmpty())
	{
		FVector2D locationToDestroy;
		pendingChunksToDestroy.Dequeue(locationToDestroy);
		AChunk* chunkToDestroy = chunkMap_Size_1.FindRef(locationToDestroy);
		if (chunkToDestroy->isFinished)
		{
			chunkToDestroy->K2_DestroyActor();
			chunkMap_Size_1.Remove(locationToDestroy);
		}
		else
		{
			pendingChunksToDestroy.Enqueue(locationToDestroy);
		}
	}
}

/*
* Adds a chunk to a pending que for deletion / removal from the world
* @param IN_ChunkLocation - the location of the chunk in chunk units to be removed
* @return bool Will return true if sucsessfull otherwise false
*/
bool ChunkManager::AddToPendingChunks(FVector2D IN_ChunkLocation)
{
	if (chunkMap_Size_1.Contains(IN_ChunkLocation))
	{
		AChunk* pendingChunk = chunkMap_Size_1.FindRef(IN_ChunkLocation);
		if (!pendingChunk->isPendingDestruction)
		{
			pendingChunk->isPendingDestruction = true;
			pendingChunksToDestroy.Enqueue(IN_ChunkLocation);
			return true;
		}
	}
	return false;
}

/*
* Generates chunks in a spiral out from the player
* @param IN_Location - The location of the player in chunks
* @param IN_Size - The size of the spiral to generate(view distance)
*/
void ChunkManager::SpiralGeneration(FVector2D IN_Location, int32 IN_Size)
{
	FVector2D location = FVector2D(IN_Location.X, IN_Location.Y);

	int32 x = 0;
	int32 y = 0;
	int32 N = IN_Size * 2;
	int32 M = IN_Size * 2;

	int32 end = std::max(N, M) * std::max(N, M);
	for (int i = 0; i < end; ++i)
	{
		// Translate coordinates and mask them out.
		int xp = x + N / 2;
		int yp = y + M / 2;
		if (xp >= 0 && xp < N && yp >= 0 && yp < M)
		{
			std::cout << xp << '\t' << yp << '\n';
			FVector2D current_chunk_location = FVector2D(x, y) + location;
			//Cheak if the chunk has all ready been created if it has dont do anything
			float distance = current_chunk_location.Distance(location, current_chunk_location);
			distance = abs(distance);
			if (!chunkMap_Size_1.Contains(current_chunk_location))
			{
				if (distance < IN_Size)
				{
					CreateChunk(chunk_octaves, chunk_resolution, current_chunk_location, true);
				}
			}
		}
		if (abs(x) <= abs(y) && (x != y || x >= 0))
			x += ((y >= 0) ? 1 : -1);
		else
			y += ((x >= 0) ? -1 : 1);
	}
}

/*
* Called free up memory deleting all of the chunks and maps
*/
void ChunkManager::Destroy()
{
	chunkMap_Size_1.Empty();
	TArray<AChunk*> chunksToRemove;
	chunkMap_Size_1.GenerateValueArray(chunksToRemove);

	for (int32 i = 0; i <= chunksToRemove.Num(); i++)
	{
		AChunk* chunkToRemove = chunksToRemove[i];
		chunkToRemove->Destroy();
	}

	chunkMap_Size_1.Empty();
	chunksToRemove.Empty();
}