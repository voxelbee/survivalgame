// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Core/Threading/ThreadTaskGraph.h"
#include "Core/Chunks/Chunk.h"

/**
 * 
 */
class SURVIVAL_GAME_2_API ChunkManager
{
public:
	ChunkManager(ThreadTaskGraph* TASK_GRAPH_IN, UWorld* IN_WORLD, int32 IN_SEED, int32 IN_CHUNK_RESOLUTION, int32 IN_CHUNK_OCTAVES, int32 IN_VIEW_DISTANCE, float IN_ZOOM_LEVEL);
	~ChunkManager();

	//Task manager to send the tasks to
	ThreadTaskGraph* taskManager;

	//The world used to get all of the player actors and spawn the chunks
	UWorld* world;

	//The old loaction of the player stored to compare against the new location read
	FVector old_player_location;

	//The old loaction of the player stored to compare against the new location read
	FVector2D old_player_chunk_location;

	//The seed of the world
	int32 seed;

	//The view distance
	int32 view_distance;

	//The resolution of the chunks
	int32 chunk_resolution;

	//The octaces of the chunks
	int32 chunk_octaves;

	//The zoom level into the height map
	float zoom_level;

	//The current chunk
	AChunk* currentChunk;

	/*
	* An updater called every frame to remove chunks from the world
	* @param In_DeltaTime - The delta time of the frame
	*/
	void Update(float In_DeltaTime);

	/*
	* Spawns a chunk into the world
	* @param IN_Location - The location of the chunk in world units
	* @return The pointer to the chunk
	*/
	AChunk* SpawnChunk(FVector &location);

	//chunk map used to store all of the AChunks
	TMap < FVector2D, AChunk* > chunkMap_Size_1;

	//chunk map used to store all of the AChunks
	TMap < FVector2D, AChunk* > chunkMap_Size_4;

	//chunk map used to store all of the AChunks
	TMap < FVector2D, AChunk* > chunkMap_Size_16;

	//chunk map used to store all of the AChunks
	TMap < FVector2D, AChunk* > chunkMap_Size_256;

	/*
	* A simple function to create a chunk and add it to the world
	* @param IN_Octaves - the octaves of the height map to be generated
	* @param IN_TerrainSize - The amount of vertices the chunk will have allong one side eg. 16 will be 16 x 16
	* @param IN_ChunkLocation - The location of the chunk in chunk units
	* @param IN_IsNewChunk - True if you whant to spawn a new chunk
	*/
	void CreateChunk(int32 IN_Octaves, int32 IN_TerrainSize, FVector2D IN_ChunkLocation, bool IN_IsNewChunk);

	/*
	* An updater called every frame to remove chunks from the world
	*/
	void RemoveChunksUpdate(FVector2D IN_Location, int32 IN_Size);

	/*
	* Adds a chunk to a pending que for deletion / removal from the world
	* @param IN_ChunkLocation - the location of the chunk in chunk units
	* @return bool Will return true if sucsessfull otherwise false
	*/
	bool AddToPendingChunks(FVector2D IN_ChunkLocation);

	//A que of chunks that are going to be removed from the world
	TQueue<FVector2D> pendingChunksToDestroy;

	/*
	* Generates chunks in a spiral out from the player
	* @param IN_Location - The location of the player
	* @param IN_Size - The size of the spiral to generate(view distance)
	*/
	void SpiralGeneration(FVector2D IN_Location, int32 IN_Size);

	/*
	* Called free up memory deleting all of the chunks and maps
	*/
	void Destroy();
};
