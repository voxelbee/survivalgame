// Fill out your copyright notice in the Description page of Project Settings.

#include "MainManager.h"
#include "Survival_Game_2.h"
#include "Core/Managers/Chunks/ChunkManager.h"
#include "Core/Threading/ThreadTaskGraph.h"


// Sets default values
AMainManager::AMainManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AMainManager::BeginPlay()
{
	Super::BeginPlay();
	
	//Create the managers:

	taskManager = new ThreadTaskGraph();
	chunkManager = new ChunkManager(taskManager, GetWorld(), seed, chunk_octaves, chunk_resolution, view_distance, zoom_level);
}

// Called every frame
void AMainManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Update the managers:
	chunkManager->Update(DeltaTime);
	taskManager->MainThreadUpdate(DeltaTime);
}

//Called when the actor is destroyed
void AMainManager::Destroy()
{
	//Clean up the managers on the game being closed:
	chunkManager->Destroy();
	taskManager->Shutdown();

	delete taskManager;
	taskManager = NULL;

	delete chunkManager;
	chunkManager = NULL;
}
