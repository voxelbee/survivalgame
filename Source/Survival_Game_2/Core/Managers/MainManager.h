// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
class ChunkManager;
class ThreadTaskGraph;






#include "GameFramework/Actor.h"
#include "MainManager.generated.h"

UCLASS()
class SURVIVAL_GAME_2_API AMainManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMainManager();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	//Called when the actor is destroyed
	virtual void Destroy();

	//The seed of the world
	UPROPERTY(EditAnywhere, Category = Terrain)
	int32 seed = 1;

	//The view distance
	UPROPERTY(EditAnywhere, Category = Terrain)
	int32 view_distance = 20;

	//The resolution of the chunks
	UPROPERTY(EditAnywhere, Category = Terrain)
	int32 chunk_resolution = 8;

	//The octaces of the chunks
	UPROPERTY(EditAnywhere, Category = Terrain)
	int32 chunk_octaves = 16;

	//The zoom level into the height map
	UPROPERTY(EditAnywhere, Category = Terrain)
	float zoom_level = 0.01;

	//All of the managers:

	ChunkManager* chunkManager;
	ThreadTaskGraph* taskManager;

};
