// Fill out your copyright notice in the Description page of Project Settings.

#include "ThreadWorker.h"
#include "Survival_Game_2.h"
#include "Core/Threading/ThreadTaskGraph.h"
#include "Core/Threading/ThreadDataContainer.h"

ThreadWorker::ThreadWorker(ThreadTaskGraph* IN_TASK_GRAPH, int32 IN_THREAD_ID)
	: taskGraph(IN_TASK_GRAPH)
	, StopTaskCounter(0)
	, ThreadID(IN_THREAD_ID)
{
	Thread = FRunnableThread::Create(this, TEXT("Survival_ThreadWorker: ") + ThreadID, 0, TPri_BelowNormal); //windows default = 8mb for thread, could specify more
}

ThreadWorker::~ThreadWorker()
{
	EnsureCompletion();

	delete Thread;
	Thread = NULL;

	delete workingData;
	workingData = NULL;
}

uint32 ThreadWorker::Run()
{
	workingData = NULL;

	while (StopTaskCounter.GetValue() == 0)
	{
		if (workingData != NULL)
		{
			workingData->DoTask();
			workingData->SendData();
			ThreadDataContainer* tempData = workingData;
			workingData = NULL;

			taskGraph->FinishedTask(ThreadID, tempData);
		}
		FPlatformProcess::Sleep(0.03);
	}

	return 0;
}

void ThreadWorker::Stop()
{
	StopTaskCounter.Increment();
}

void ThreadWorker::EnsureCompletion()
{
	Stop();
	Thread->WaitForCompletion();
}

void ThreadWorker::Shutdown()
{
	EnsureCompletion();
}

bool ThreadWorker::Init()
{
	return true;
}