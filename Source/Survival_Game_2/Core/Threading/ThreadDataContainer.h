// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * 
 */
class SURVIVAL_GAME_2_API ThreadDataContainer
{
public:
	ThreadDataContainer();
	virtual ~ThreadDataContainer();

	//The task for the thread to execute
	virtual void DoTask() = 0;

	//Called when the thread has finished do task
	//this sends the data to where it needs to go eg. the chunk
	virtual void SendData() = 0;

	//This is for sending data that has to be set on the MAIN THREAD!
	virtual void SendDataMainThread() = 0;
};

