// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
class ThreadDataContainer;
class ThreadWorker;

/**
 * 
 */
class SURVIVAL_GAME_2_API ThreadTaskGraph : public FRunnable
{
public:
	/** Thread to run the worker FRunnable on */
	FRunnableThread* Thread;

	/** Stop this thread? Uses Thread Safe Counter */
	FThreadSafeCounter StopTaskCounter;

	bool isFinished;
	//~~~ Thread Core Functions ~~~

	//Constructor / Destructor
	ThreadTaskGraph();
	virtual ~ThreadTaskGraph();

	// Begin FRunnable interface.
	virtual bool Init();
	virtual uint32 Run();
	virtual void Stop();
	// End FRunnable interface

	/** Makes sure this thread has stopped properly */
	void EnsureCompletion();

	//~~~ Starting and Stopping Thread ~~~

	/** Shuts down the thread. Static so it can easily be called from outside the thread context */
	void Shutdown();

	TQueue<int32> ReadyThreads;
	TArray<ThreadWorker*> Threads;

	TQueue<ThreadDataContainer*> WaitingData;
	TQueue<ThreadDataContainer*> MainThreadTasksData;

	void FinishedTask(int32 IN_THREAD_ID, ThreadDataContainer* IN_DATA_CONTAINER);

	void Update(float DeltaTime);

	void MainThreadUpdate(float DeltaTime);

	void AddTask(ThreadDataContainer* IN_DATA_CONTAINER);

	int32 maxThreadCount;
};
