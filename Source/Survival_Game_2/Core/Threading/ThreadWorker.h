// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
class ThreadDataContainer;
class ThreadTaskGraph;

/**
 * 
 */
class SURVIVAL_GAME_2_API ThreadWorker : public FRunnable
{
public:

	/** Thread to run the worker FRunnable on */
	FRunnableThread* Thread;

	ThreadTaskGraph* taskGraph;

	/** Stop this thread? Uses Thread Safe Counter */
	FThreadSafeCounter StopTaskCounter;

private:

	int32 ThreadID;
public:
	ThreadDataContainer* workingData;

	bool isFinished;
	//~~~ Thread Core Functions ~~~

	//Constructor / Destructor
	ThreadWorker(ThreadTaskGraph* IN_TASK_GRAPH, int32 IN_THREAD_ID);
	virtual ~ThreadWorker();

	// Begin FRunnable interface.
	virtual bool Init();
	virtual uint32 Run();
	virtual void Stop();
	// End FRunnable interface

	/** Makes sure this thread has stopped properly */
	void EnsureCompletion();

	//~~~ Starting and Stopping Thread ~~~

	/** Shuts down the thread. Static so it can easily be called from outside the thread context */
	void Shutdown();
};
