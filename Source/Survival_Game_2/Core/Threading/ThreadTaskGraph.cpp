// Fill out your copyright notice in the Description page of Project Settings.

#include "ThreadTaskGraph.h"
#include "Survival_Game_2.h"
#include "Core/Threading/ThreadDataContainer.h"
#include "Core/Threading/ThreadWorker.h"
#include <time.h>
#include <thread>

ThreadTaskGraph::ThreadTaskGraph()
	: StopTaskCounter(0)
{
	maxThreadCount = std::thread::hardware_concurrency();

	if (maxThreadCount <= 0)
	{
		UE_LOG(CriticalErrors, Fatal, TEXT("You need 2 or more cores to run the game!"));
	}

	Thread = FRunnableThread::Create(this, TEXT("Survival_ThreadTaskGraph"), 0, TPri_BelowNormal); //windows default = 8mb for thread, could specify more
}

ThreadTaskGraph::~ThreadTaskGraph()
{
	for (int32 i = 0; i < Threads.Num(); i++)
	{
		ThreadWorker* threadToDelete = Threads.Last(0);
		threadToDelete->Shutdown();
		delete threadToDelete;
		threadToDelete = NULL;
		Threads.RemoveAt(Threads.Num() - 1);
	}

	delete Thread;
	Thread = NULL;
}

uint32 ThreadTaskGraph::Run()
{
	while (StopTaskCounter.GetValue() == 0)
	{
		FPlatformProcess::Sleep(0.001);
		Update(0);
	}

	return 0;
}

void ThreadTaskGraph::Stop()
{
	StopTaskCounter.Increment();
}

void ThreadTaskGraph::EnsureCompletion()
{
	Stop();
	Thread->WaitForCompletion();
}

void ThreadTaskGraph::Shutdown()
{
	EnsureCompletion();
}

bool ThreadTaskGraph::Init()
{
	return true;
}

void ThreadTaskGraph::FinishedTask(int32 IN_THREAD_ID, ThreadDataContainer* IN_DATA_CONTAINER)
{
	ReadyThreads.Enqueue(IN_THREAD_ID);
	MainThreadTasksData.Enqueue(IN_DATA_CONTAINER);
}

void ThreadTaskGraph::MainThreadUpdate(float DeltaTime)
{
	clock_t start_time;
	clock_t current_time;

	start_time = clock();
	current_time = clock();

	while (!MainThreadTasksData.IsEmpty())
	{
		ThreadDataContainer* data;
		MainThreadTasksData.Dequeue(data);
		data->SendDataMainThread();
		current_time = clock();
		if (current_time > start_time + (CLOCKS_PER_SEC) / 60)
		{
			return;
		}
	}
}

void ThreadTaskGraph::Update(float DeltaTime)
{
	if (WaitingData.IsEmpty())
	{
		return;
	}

	if (ReadyThreads.IsEmpty())
	{
		if (maxThreadCount == Threads.Num())
		{
			return;
		}
		ThreadWorker* newThread = new ThreadWorker(this, Threads.Num() + 1);
		Threads.Add(newThread);
		ReadyThreads.Enqueue(Threads.Num());
	}
	//Get the data off the que.
	ThreadDataContainer* data;
	WaitingData.Dequeue(data);

	//Get the ID of the free thread.
	int32 threadID;
	ReadyThreads.Dequeue(threadID);

	//Get the thread from the ID that we just got.
	ThreadWorker* thread;

	//We take away on because arrays start at 0.
	thread = Threads[threadID - 1];

	thread->workingData = data;
}

void ThreadTaskGraph::AddTask(ThreadDataContainer* IN_DATA_CONTAINER)
{
	WaitingData.Enqueue(IN_DATA_CONTAINER);
}

