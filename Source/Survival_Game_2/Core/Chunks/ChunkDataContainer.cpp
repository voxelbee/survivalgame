// Fill out your copyright notice in the Description page of Project Settings.

#include "ChunkDataContainer.h"
#include "Survival_Game_2.h"
#include "Core/Noise/noise.h"
#include "Core/Noise/noiseutils.h"
#include "KismetProceduralMeshLibrary.h"
#include "Core/Chunks/Chunk.h"
#include <algorithm>

ChunkDataContainer::ChunkDataContainer()
{
	minHeight = MAX_flt;
}

ChunkDataContainer::~ChunkDataContainer()
{
}

//The task for the thread to execute
void ChunkDataContainer::DoTask()
{
	heightMapL.Clear(0);

	double x = chunkLocation.X * zoomLevel;
	double y = chunkLocation.Y * zoomLevel;

	float fTerrainSize = float(terrainSizeL);
	float additonalSize = (zoomLevel) * (fTerrainSize / (fTerrainSize - 1));

	module::RidgedMulti terrain;
	terrain.SetFrequency(2);
	terrain.SetSeed(chunkSeed);
	terrain.SetOctaveCount(terrainOctaves);

	utils::NoiseMapBuilderPlane planetL;
	planetL.SetBounds(x - (additonalSize / terrainSizeL), x + additonalSize + (additonalSize / terrainSizeL), y - (additonalSize / terrainSizeL), y + additonalSize + (additonalSize / terrainSizeL));
	planetL.SetDestSize(terrainSizeL + 2, terrainSizeL + 2);
	planetL.SetSourceModule(terrain);
	planetL.SetDestNoiseMap(heightMapL);
	planetL.Build();

	GenerateMesh();

	//Create the mesh section within the procedural mesh
	//UKismetProceduralMeshLibrary::CalculateTangentsForMesh(vertices, triangles, uvs, normals, tangents);
	//mesh->bUseAsyncCooking = true;
	//Set the chunk to make dynamic shadows
	//mesh->bCastDynamicShadow = true;
	//Make the chunk do 2 sided shadows so that if the sun goes under the ground at night it still shadows correctly
	//mesh->bCastShadowAsTwoSided = true;
	//Make the chunk cast shadows at a distance
    //mesh->bCastFarShadow = true;
}

//Called when the thread has finished do task
//this sends the data to where it needs to go eg. the chunk
void ChunkDataContainer::SendData()
{
	//Transfer all of the generated mesh and variables to the chunk
	//chunk->terrainMesh = mesh;
	chunk->chunkLocation = chunkLocation;
	chunk->terrainSize = terrainSizeL;
}

//Generates the vertices ect...
void ChunkDataContainer::GenerateMesh()
{
	int32 heightMapSize = terrainSizeL;
	int32 numVerts = (heightMapSize + 2) * (heightMapSize + 2);

	Height0 = heightMapL.GetValue(0, 0);

	int vertexIndex = 0;

	triangles.Reset();

	vertices.Reset();
	vertices.AddUninitialized(numVerts);

	uvs.Reset();
	uvs.AddUninitialized(numVerts);

	vertexColors.Reset();
	vertexColors.AddUninitialized(numVerts);

	normals.Reset();
	normals.AddZeroed(numVerts);

	for (int32 i = 0; i < heightMapSize + 2; i++)
	{
		for (int32 j = 0; j < heightMapSize + 2; j++)
		{
			float x = i / (heightMapSize - 1.0f);
			float y = j / (heightMapSize - 1.0f);
			float z = heightMapL.GetValue(i, j) * 10;

			FVector p0 = FVector(x, y, z);

			FVector2D uVp0 = FVector2D(i, j) / heightMapSize;

			FLinearColor vCp0 = FLinearColor(120, 144, 72, 0);

			int vertIndex1 = vertexIndex++;

			uvs[vertIndex1] = uVp0;
			vertexColors[vertIndex1] = vCp0;
			vertices[vertIndex1] = p0;
		}
	}

	for (int32 i = 0; i < heightMapSize + 1; i++)
	{
		for (int32 j = 0; j < heightMapSize + 1; j++)
		{
			int32 vertIndex1 = j + i * (heightMapSize + 2);
			int32 vertIndex2 = j + i * (heightMapSize + 2) + 1;
			int32 vertIndex3 = j + (i + 1) * (heightMapSize + 2);
			int32 vertIndex4 = j + (i + 1) * (heightMapSize + 2) + 1;

			FVector v1 = vertices[vertIndex1];
			FVector v2 = vertices[vertIndex2];
			FVector v3 = vertices[vertIndex3];
			FVector v4 = vertices[vertIndex4];

			FVector n1 = -FVector::CrossProduct(v2 - v1, v3 - v1);
//			FVector n2 = FVector::CrossProduct(v2 - v4, v3 - v4);
			FVector n2 = FVector(0, 0, 0);

			normals[vertIndex1] += n1 + n2;
			normals[vertIndex2] += n1 + n2;
			normals[vertIndex3] += n1 + n2;
			normals[vertIndex4] += n1 + n2;

			if (i == 0 || j == 0 || i == heightMapSize || j == heightMapSize)
				continue;

			triangles.Add(vertIndex1);
			triangles.Add(vertIndex2);
			triangles.Add(vertIndex3);

			triangles.Add(vertIndex2);
			triangles.Add(vertIndex4);
			triangles.Add(vertIndex3);
		}
	}

	for (int i = 0; i < normals.Num(); i++)
		normals[i].Normalize();
}

//This is for sending data that has to be set on the MAIN THREAD!
void ChunkDataContainer::SendDataMainThread()
{
	chunk->ReceiveData(vertices, triangles, uvs, normals, tangents, vertexColors);
}