// Fill out your copyright notice in the Description page of Project Settings.

#include "Chunk.h"
#include "Survival_Game_2.h"
#include "Core/Noise/noiseutils.h"
//#include "Developer/ImageWrapper/Public/Interfaces/IImageWrapper.h"
//#include "Developer/ImageWrapper/Public/Interfaces/IImageWrapperModule.h"


// Sets default values
AChunk::AChunk()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	terrainMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GeneratedMesh"));
	RootComponent = terrainMesh;

	bReplicates = true;
	static ConstructorHelpers::FObjectFinder<UMaterial> TerrainMaterial(TEXT("Material'/Game/Environment/Terrain/Materials/M_Terrain.M_Terrain'"));

	if (TerrainMaterial.Object != NULL)
	{
		terrainMaterial = TerrainMaterial.Object;
	}
}

// Called when the game starts or when spawned
void AChunk::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AChunk::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

//Reciveing data from the thread HAS TO BE CALLED ON MAIN THREAD!
void AChunk::ReceiveData(TArray<FVector> vertices,
TArray<int32> triangles,
TArray<FVector2D> uvs,
TArray<FVector> normals,
TArray<FProcMeshTangent> tangents,
TArray<FLinearColor> vertexColors)
{
	//this->UnregisterAllComponents();

	terrainMesh->CreateMeshSection_LinearColor(0, vertices, triangles, normals, uvs, vertexColors, tangents, true);

	terrainMesh->bUseAsyncCooking = true;
	//Set the chunk to make dynamic shadows
	terrainMesh->bCastDynamicShadow = true;
	//Make the chunk do 2 sided shadows so that if the sun goes under the ground at night it still shadows correctly
	terrainMesh->bCastShadowAsTwoSided = true;
	//Make the chunk cast shadows at a distance
	terrainMesh->bCastFarShadow = true;

	//Add the terrain mesh to the root of the actor
	//terrainMesh->RegisterComponent();

	terrainMesh->SetRelativeLocation(FVector(chunkLocation.X * 3200, chunkLocation.Y * 3200, 50000));
	//Seting the size of the chunk to 16m
	terrainMesh->SetRelativeScale3D(FVector(3200, 3200, 3200));

	//Add the terrain mesh to the root of the actor
	//terrainMesh->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::KeepWorldTransform);
	terrainMesh->Activate();

	UMaterialInstanceDynamic* material_Dyn_00 = UMaterialInstanceDynamic::Create(terrainMaterial, this);
	terrainMesh->SetMaterial(0, material_Dyn_00);

	isFinished = true;
}