// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "GameFramework/Actor.h"
#include "Core/Noise/noiseutils.h"
#include "ProceduralMeshComponent.h"
#include "Core/Noise/noiseutils.h"
#include "Chunk.generated.h"

UCLASS()
class SURVIVAL_GAME_2_API AChunk : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AChunk();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	//Reciveing data from the thread HAS TO BE CALLED ON MAIN THREAD!
	void ReceiveData(TArray<FVector> vertices,
	TArray<int32> triangles,
	TArray<FVector2D> uvs,
	TArray<FVector> normals,
	TArray<FProcMeshTangent> tangents,
	TArray<FLinearColor> vertexColors);

	//True when the chunk is fully generated
	bool isFinished;

	//True when the chunk has been marked for destruction
	bool isPendingDestruction;

	//True when part of the chunk is under the water
	bool isNeedingWater;

	//The mesh for the terrain of the chunk
	UPROPERTY(VisibleAnywhere)
	UProceduralMeshComponent* terrainMesh;

	//The location of the chunk in chunk units.
	FVector2D chunkLocation;

	//TerrainSize mostly efects the quality of the chunk it is how meany pixels in width/height the height map will be
	//and how many vertices the finshed mesh will have in width/height
	int32 terrainSize;

	//The lookUp array storing the heights of the chunk
	TArray<float> HeightLookUp;

	//Height of the terrain at location 0, 0
	float height00;

	UMaterial* terrainMaterial;
	UMaterial* waterMaterial;

	utils::NoiseMap heightMapH;

	UStaticMesh* waterMesh;

	//The biome ids per index of the terrain mesh
	TArray<int32> terrainBiomeIDs;
};
