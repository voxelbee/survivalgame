// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Core/Noise/noiseutils.h"
#include "ProceduralMeshComponent.h"
#include "Core/Threading/ThreadDataContainer.h"
#include "Core/Chunks/Chunk.h"

/**
 * 
 */
class SURVIVAL_GAME_2_API ChunkDataContainer : public ThreadDataContainer
{
public:
	ChunkDataContainer();
	~ChunkDataContainer();

	//The task for the thread to execute
	void DoTask();

	//Called when the thread has finished do task
	//this sends the data to where it needs to go eg. the chunk
	void SendData();
	
	//This is for sending data that has to be set on the MAIN THREAD!
	void SendDataMainThread();

	//Generates the vertices ect...
	void GenerateMesh();

	//Noise generation variables:

	FVector2D chunkLocation;
	int32 chunkSeed;
	float zoomLevel;
	int32 terrainSizeL;
	int32 terrainOctaves;
	utils::NoiseMap heightMapL;
	AChunk* chunk;

	//Mesh variables:
	TArray<FVector> vertices;
	TArray<int32> triangles;
	TArray<FVector2D> uvs;
	TArray<FVector> normals;
	TArray<FProcMeshTangent> tangents;
	TArray<FLinearColor> vertexColors;
	TArray<int32> biomeIDs;

	float minHeight;
	float maxHeight;
	float Height0;

	//The terrain mesh to create and fill with data
	//UProceduralMeshComponent* mesh;
};
