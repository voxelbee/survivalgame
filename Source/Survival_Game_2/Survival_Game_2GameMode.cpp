// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Survival_Game_2GameMode.h"
#include "Survival_Game_2Character.h"
#include "UObject/ConstructorHelpers.h"

ASurvival_Game_2GameMode::ASurvival_Game_2GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
