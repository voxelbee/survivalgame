// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Survival_Game_2 : ModuleRules
{
	public Survival_Game_2(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "ProceduralMeshComponent" });
        //PrivateDependencyModuleNames.AddRange(new string[] { "ProceduralMeshComponent" });
        //PrivateIncludePathModuleNames.AddRange(new string[] { "ProceduralMeshComponent" });
    }
}
