// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Survival_Game_2.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Survival_Game_2, "Survival_Game_2" );

//General Log
DEFINE_LOG_CATEGORY(GeneralLog);

//Logging during game startup
DEFINE_LOG_CATEGORY(Init);

//Logging for your AI system
DEFINE_LOG_CATEGORY(AI);

//Logging for Critical Errors that must always be addressed
DEFINE_LOG_CATEGORY(CriticalErrors);