// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "EngineMinimal.h"
#include "Engine.h"

//General Log
DECLARE_LOG_CATEGORY_EXTERN(GeneralLog, Log, All);

//Logging during game startup
DECLARE_LOG_CATEGORY_EXTERN(Init, Log, All);

//Logging for your AI system
DECLARE_LOG_CATEGORY_EXTERN(AI, Log, All);

//Logging for Critical Errors that must always be addressed
DECLARE_LOG_CATEGORY_EXTERN(CriticalErrors, Log, All);